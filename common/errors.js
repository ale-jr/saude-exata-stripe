
exports.ForbiddenError = (resp) => {
  resp.status(403).json({message:"Permission denied"})
}

const customError = (code,message,json) => {
  const error =  new Error(message)
  error.json = {...json, message}
  error.http_code = code
  return error
}
exports.customError = customError

exports.handleError = (error,resp) => {

  //Erro geral do MongoDB
  if(error.name === 'MongoError'){
    resp.status(500).json({
      message: 'An error ocurred while connecting to the database'
    })
  }
  //Erro de validaçao de dados do mongoDB
  else if(error.name === 'ValidationError'){
    resp.status(400).json({message:error.message})
  }
  //Erro do Stripe
  else if(error.type !== undefined && error.type.indexOf('Stripe') > -1){
    switch(error.type){
      case 'StripeCardError':
        resp.status(400).json({
          message: error.message,
          decline_code: error.raw.decline_code
        })
      break;
      case 'StripeInvalidRequestError':
        resp.status(400).json({
          message: 'Invalid parameters from Payment API'
        })
      break;
      case 'StripeAPIError':
        resp.status(500).json({
          message: 'An error occurred internally with Payment API'
        })
      break;
      case 'StripeConnectionError':
        resp.status(500).json({
          message: 'An error occurred while connecting with Payment API'
        })
      break;
      case 'StripeAuthenticationError':
      case 'StripePermissionError':
        resp.status(500).json({
          message: 'An error occurred while authenticating with Payment API'
        })
      break;
      case 'StripeRateLimitError':
        resp.status(500).json({
          message: 'Too many requests to Payment API, wait and try again'
        })
      break;

      default:
        resp.status(500).json({
          message: 'Unexpected error from payment API'
        })
    }
  }
  //Se há algum código de erro ou json para exibir
  else if(error.http_code){
    if(error.json){
      resp.status(error.http_code).json(error.json)
    }
    else{
      resp.status(error.http_code).end()
    }

  }
  //Erro desconhecido
  else{
    resp.status(500).json({
      message: `An unexpected error occurred: ${error.message}`
    })
  }
}
