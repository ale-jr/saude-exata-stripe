//Variáveis de ambiente do servidor
exports.server = {
  port: process.env.SERVER_PORT || 3000,

}

exports.logs = {
  name: process.env.LOGS_NAME || 'default_log',
  level: process.env.LOGS_LEVEL || 'info',
  //Definir path caso queira que os logs sejam enviados para um arquivo
  path: process.env.LOGS_PATH,
  rotating_file: process.env.LOGS_ROTATING_FILE,
  //Caso o stream seja rotativo, definir essas variáveis
  period: process.env.LOGS_PERIOD || '1d',
  count: process.env.LOGS_COUNT || 3
}

exports.db = {
  url: process.env.DB_URL,
  prefix: process.env.DB_PREFIX
}

exports.security = {
  jwt_secret: process.env.SECURITY_JWT_SECRET,
}

exports.stripe = {
  secret_key : process.env.STRIPE_SECRET_KEY,
  publishable_key : process.env.STRIPE_PUBLISHABLE_KEY,
  currency: process.env.STRIPE_CURRENCY,
  webhook_url: process.env.STRIPE_WEBHOOK_URL,

}
