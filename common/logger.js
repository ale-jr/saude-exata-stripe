const bunyan = require('bunyan')
const {logs} = require('./environment')

exports.logger = bunyan.createLogger({
  name: logs.name,
  streams: [
    {
      stream: logs.path || process.stdout,
      level: logs.level,
      path: logs.path,
      type: logs.rotating_file ? 'rotating-file' : 'file',
      period: logs.period,
      count: logs.count
    }
  ]
})
