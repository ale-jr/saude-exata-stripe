
//Classe responsável por configurar e iniciar o Express e conectar no MongoDB
const Server = require('./server/server')

//Import dos endpoints, inserir esses objetos no array para iniciar as rotas
const customersRouter = require('./api/customers/router')
const cardsRouter = require('./api/cards/router')
const chargesRouter = require('./api/charges/router')
const plansRouter = require('./api/plans/router')
const subscriptionsRouter = require('./api/subscriptions/router')
const webhookRouter = require('./api/webhooks/router')
//Inicializar webhook
const {setupWebhook} = require('./api/webhooks/initialize')

//Logs
const {logger} = require('./common/logger')


const server = new Server()

//Iniciar servidor e DB
server.init([
  customersRouter,
  cardsRouter,
  chargesRouter,
  plansRouter,
  subscriptionsRouter,
  webhookRouter
])
.then(async ()=>{
  logger.info("Servidor HTTP iniciado e banco de dados conectado ;)")
  //Atualiza o webhook do Stripe
  await setupWebhook()
})
.catch((error)=>{
  logger.fatal(error,"Ocorreu um erro ao iniciar a aplicação")
})
