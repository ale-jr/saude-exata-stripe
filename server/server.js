const express = require('express')

//Logs e variáveis de ambiente
const { logger } = require('../common/logger')
const { server, db } = require('../common/environment')

//Banco de dados
const mongoose = require('mongoose')

//Importando middlewares
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const {tokenParser} = require('../security/token.parser')
const errorMiddleware = require('./error.handler')
const {rawBodyMiddleware} = require('../common/utils')


class Server {

  //Iniciar Mongo (retorna uma promise)
  initializeDb() {
    return mongoose.connect(db.url,{
      useNewUrlParser: true
    })
  }

  //Iniciar Express (retorna uma promise)
  initializeServer(routers = []) {
    return new Promise((resolve,reject)=>{
      try{
        this.application = express()

        //Aqui vão os middlewares
        //Obter o req.body em forma de texto para validação do webhook do Stripe, pois eles só validam a assinatura se o corpo estiver em texto puro
        this.application.use(rawBodyMiddleware);
        this.application.use(bodyParser.urlencoded({extended: true}))
        this.application.use(bodyParser.json())
        this.application.use(methodOverride())
        //Middleware para extrair usuário do token JWT
        this.application.use(tokenParser)

        //QUESTION: Inserir middleware para CORS?

        //Adicionar todas as rotas
        routers.forEach(({path,router})=>{
          this.application.use(path,router)
        })

        //Middleware para exibir os erros
        this.application.use(errorMiddleware)

        //Iniciar servidor HTTP
        this.application.listen(server.port,()=>{
          resolve(this.application)
        })
      }
      catch(e){
        reject(e)
      }
    })
  }

  //inicia db e o express
  init(routers = []){
    return this.initializeDb()
           .then(()=>this.initializeServer(routers)
            .then(()=>this))
  }

  //mata tudo
  shutdown(){
    return mongoose.disconnect().then(()=>this.application.close())
  }
}

module.exports = Server
