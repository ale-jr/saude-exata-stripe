//Lida com os erros enviados

//Logs
const { logger } = require('../common/logger')
//Erros e error handler
const { handleError } = require('../common/errors')

//middleware para gerenciar erros
module.exports =  (error, req, resp, next) => {
  handleError(error,resp)
}
