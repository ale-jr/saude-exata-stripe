//Rotas do express
const router = require('express').Router()

//Logs
const {logger} = require('../../common/logger')

const enabled_events = ['invoice.payment_failed']


//Erros e error handler
const { handleError } = require('../../common/errors')


//Middleware para autorização
const { authorize, authorizeAdmin } = require('../../security/authz.handler')

//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

let secret = undefined

//Definir webhook
const setupWebhook = async () => {

    try{
      //Achar os webhooks
      const endpoints = await stripeApi.webhookEndpoints.list({limit:100})
      let matchEndpoint = ''
      endpoints.data.some((endpoint)=>{
        if(endpoint.url == stripe.webhook_url){
          matchEndpoint = endpoint
          return true
        }
        return false
      })
      //Caso webhook exista, excluir o mesmo
      if(matchEndpoint){
        const deleteEndpoint = await stripeApi.webhookEndpoints.del(matchEndpoint.id)
      }

      //Adicionar o webhook
      const newWebhook = await stripeApi.webhookEndpoints.create({
        url: stripe.webhook_url,
        enabled_events
      })

      //Define o secret para verificar a assinatura
      secret = newWebhook.secret
      logger.info("Webhook criado")
      return true
  }
  catch(error){
    logger.error(error,"Erro o criar webhook")
    return false
  }
}

//Obtem a assinatura
const getSecret = () => secret


exports.setupWebhook = setupWebhook
exports.getSecret = getSecret
