//Rotas do express
const router = require('express').Router()

const bodyParser = require('body-parser')
const contentType = require('content-type')
const getRawBody = require('raw-body')

const { logger } = require('../../common/logger')
//Erros e error handler
const { handleError } = require('../../common/errors')


//Middleware para autorização
const { authorize, authorizeAdmin } = require('../../security/authz.handler')

//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

//Obtem a assinatura
const {getSecret} = require('./initialize')

//Caminho das rotas
const path = '/webhook'

//Endpoint do webhook
const webhookEndpoint = (req,resp,next) => {

  const signature = req.headers['stripe-signature']
  let event;
  //definir evento e verificar assinatura
  try{
    const secret = getSecret()
    event = stripeApi.webhooks.constructEvent(req.rawBody,signature,secret)
  }
  catch(error){
    //Enviar detalhes do erro para conseguirmos ver nos logs do Stripe
    resp.status(400).send(`Webhook Error: ${error.message}`)
    //Logar para mais detalhes
    logger.warn(`Webhook Error: ${error.message}`)
    return true
  }

  switch(event.type){
    //Pagamento da inscrição falhou
    case 'invoice.payment_failed':
    handleInvoicePaymentFailed(event.data.object)
    break;
    //Tipo não reconhecido, enviar um erro
    default:
      res.status(400).end();
      return
  }
  //Retornar ao Stripe dizendo que está tudo certo :D
  resp.json({received: true})
}


router.post('/',webhookEndpoint)

const handleInvoicePaymentFailed = (invoice) => {
  //TODO: O que fazer quando um pagamento não é aceito
  //QUESTION: Disparar um email? Uma notificação push
}


exports.path = path
exports.router = router
