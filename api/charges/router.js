//Rotas do express
const router = require('express').Router()
//Erros e error handler
const { customError } = require('../../common/errors')
const { asyncMiddleware } = require('../../common/utils')


//Middleware para autorização
const { authorize, authorizeAdmin } = require('../../security/authz.handler')

//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

//Modelos do banco de dados
const User = require('../customers/users.model')
const Customer = require('../customers/customers.model')
const Card = require('../cards/model')
const Charge = require('./model')

const {getUser} = require('../customers/getUserInfo')


//Caminho das rotas
const path = '/charges'

//Endpoint para criar uma cobrança
const createCharge = async (req,resp,next) => {

    const {use_default,card,amount,description,statement_descriptor,userId} = req.body

    //
    if(!amount || (!use_default && !card)){
        throw customError(400,'Missing parameters')
    }

    //Obtem detalhes do usuário
    const customer = await getUser(userId)


    let source = undefined
    let cardUsed = undefined

    //Verificar se o cartão pertece ao cliente e obter o id do cartão gerado pelo stripe
    const customerHasCard = customer.cards.some((c)=>{
      if(c._id == card){
        cardUsed = c
        return true
      }
      return false
    })
    //Caso o cartão nao pertença ao cliente e não é uma cobrançausando o cartão principal
    if(!customerHasCard && !use_default){
      throw customError(400,'Card not found.')
    }

    //Selecionar cartão principal caso a cobrança seja gerada com o cartão principal
    if(use_default){
      customer.cards.some(c=>{
        if(c.default_source){
          cardUsed = c
          return true
        }
        return false
      })
    }

    //Efetua a cobrança, caso ocorra algum erro no cartão, obtem detalhes da cobrança para mais detalhes do erro
    let stripeCharge = undefined
    try{
      stripeCharge = await stripeApi.charges.create({
        amount,
        currency: stripe.currency,
        customer: customer.stripeId,
        source: cardUsed.cardId,
        description,
        statement_descriptor
      })
    }

    catch(error){
      //Caso seja um erro não relacionado ao cartão, deixar o erro passar
      if(error.type !== 'StripeCardError'){
        throw error
      }
      //Obter detalhes da cobrança, mesmo que tenha ocorrido um erro
      else{
        stripeCharge = await stripeApi.charges.retrieve(error.raw.charge)
      }
    }

    //salvar detalhes da cobrança
    const charge = new Charge({
      chargeId: stripeCharge.id,
      amount: stripeCharge.amount,
      amount_refunded: stripeCharge.amount_refunded,
      refunded: stripeCharge.refunded,
      customer: stripeCharge.customer,
      description: stripeCharge.description,
      failure_code: stripeCharge.failure_code,
      paid: stripeCharge.paid,
      payment_method: stripeCharge.payment_method,
      status: stripeCharge.status,
      userId: customer.userId,
      cardId: cardUsed._id
    })
    const savedCharge = await charge.save()

    //Retorna dados da cobrança
    resp.json(savedCharge)
}

//Endpoint para estornar cobranças
const refundCharge = async (req,resp,next) => {
    const chargeId = req.params.charge
    const amount = req.body.amount
    //Obter detalhes da cobrança
    const charge = await Charge.findById(chargeId)

    if(!charge){
      throw customError(400,'Charge not found')
    }

    if(charge.refunded){
      throw customError(400,'Charge already refunded')
    }

    //Cria o reembolso
    const stripeRefund = await stripeApi.refunds.create({
      charge: charge.chargeId,
      amount
    })

    //Obtem o novo status da cobrança
    const stripeCharge = await stripeApi.charges.retrieve(charge.chargeId)

    //Atualiza a cobrança
    const updatedCharge = await Charge.findOneAndUpdate({_id:chargeId},{
      amount: stripeCharge.amount,
      amount_refunded: stripeCharge.amount_refunded,
      refunded: stripeCharge.refunded,
      failure_code: stripeCharge.failure_code,
      paid: stripeCharge.paid,
      status: stripeCharge.status,
    },{new: true})

    //retorna os detalhes da cobrança atualizados
    resp.json(updatedCharge)
}


//Criar uma cobrança
router.post('/create',[authorizeAdmin(),asyncMiddleware(createCharge)])

//Estornar uma cobrança
router.post('/{charge}/refund',[authorizeAdmin(),asyncMiddleware(refundCharge)])

exports.router = router
exports.path = path
