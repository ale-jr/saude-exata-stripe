const mongoose = require('mongoose')
const { db } = require('../../common/environment')

const chargeSchema = new mongoose.Schema({
  chargeId : String,
  amount: Number,
  amount_refunded: Number,
  refunded: Boolean,
  customer: String,
  description: String,
  failure_code: String,
  paid: Boolean,
  payment_method: String,
  status: String,
  userId: {
    type: mongoose.ObjectId,
    ref: 'User'
  },
  cardId: {
    type: mongoose.ObjectId,
    ref: 'Card'
  }
})

chargeSchema.statics.findByStripeId = function(chargeId) {
  return this.findOne({chargeId})
}

chargeSchema.statics.findByUserId = function(userId) {
  return this.find({userId})
}


module.exports = mongoose.model('Charge',chargeSchema,`charges_${db.prefix}`)
