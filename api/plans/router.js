//Rotas do express
const router = require('express').Router()
//Erros e error handler
const { customError } = require('../../common/errors')
const { asyncMiddleware } = require('../../common/utils')


//Middleware para autorização
const { authorize, authorizeAdmin } = require('../../security/authz.handler')


//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

const {stripeProductFromPlan} = require('./products/manageProducts')
const StripePlan =  require('./stripePlans.model')

const path = '/plans'


//cria um plano no Stripe baseado em um plano existente no banco de dados
const createStripePlan = async (req,resp,next) =>{

  const {planId,interval,interval_count,amount,nickname} = req.body

  //Verifica se está tudo certo :D
  if(!planId || !interval || !interval_count || !amount){
    throw customError(400,'Missing parameters')
  }

    //Cria ou obtem o produto relacionado ao plano do Stripe
    const stripeProduct = await stripeProductFromPlan(planId)

    //Desativa planos existentes relacionados ao plano base
    await deactivatePlan(planId,stripeProduct)

    //Cria novo plano
    const stripePlan = await stripeApi.plans.create({
      amount,
      interval,
      interval_count,
      nickname,
      product: stripeProduct.stripeId,
      currency: stripe.currency,
      metadata: {
        plan: String(planId)
      }
    })

    //Obtem informações do plano no nosso banco de dados
    const plan = new StripePlan({
      stripeId: stripePlan.id,
      interval,
      interval_count,
      stripeProduct: stripeProduct.stripeId,
      amount,
      nickname,
      active: stripePlan.active,
      product: stripeProduct._id,
      plan: planId
    })
    const savedPlan = await plan.save()

    //Retorna os dados do plano
    resp.json(savedPlan)
}


//Desativa planos relacionados a um plano da coleção do mongoDB
const deactivatePlan = async (planId,productDetails) =>{
  //Obtem o produto criado com base no plano
  const stripeProduct = productDetails ? productDetails : await stripeProductFromPlan(planId)
  //Obtem os planos relacionados ao produto
  const activePlans = await stripeApi.plans.list({
    active: true,
    product: stripeProduct.stripeId
  })

  //Para cada plano ativo
  activePlans.data.forEach(async (activePlan)=>{
    //Desativar plano no Stripe
    const stripeDeactivate = await stripeApi.plans.update(activePlan.id,{active:false})
    //Atualizar mongoDB
    const updateLocalPlan = await StripePlan.update({stripeId: activePlan.id},{active:false})
  })
  return  true
}

//Desativar um plano
const deactivateStripePlan = async (req,resp,next) => {
    await deactivatePlan(req.params.plan)
    resp.status(204).end()

}



const listActivePlans = async (req,resp,next) => {

      const stripePlans = await stripeApi.plans.list({
        active: true,
        limit: 100
      })
      const activePlans = await Promise.all(stripePlans.data.map(async (stripePlan)=>{
        const plan = await StripePlan
                              .findByStripeId(stripePlan.id)
                              .populate('Product')
                              .populate('Plan')
                              .lean()
                              .exec()
        return plan
      }))
      resp.json(activePlans)
}

router.post('/',[authorizeAdmin(),asyncMiddleware(createStripePlan)])
router.get('/',[authorizeAdmin(),asyncMiddleware(listActivePlans)])
router.post('/:plan/deactivate',[authorizeAdmin(),asyncMiddleware(deactivateStripePlan)])

exports.router = router
exports.path = path
