const mongoose = require('mongoose')
const { db } = require('../../common/environment')

const planSchema = new mongoose.Schema({
  type: String,
  name: String,
  description: String,
  months: Number,
  valid: Date
})

module.exports = mongoose.model('Plan',planSchema)
