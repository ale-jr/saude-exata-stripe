const mongoose = require('mongoose')
const { db } = require('../../common/environment')

const planSchema = new mongoose.Schema({
  stripeId: String,
  interval: String,
  interval_count: Number,
  stripeProduct: String,
  amount: Number,
  nickname: String,
  active: Boolean,
  product: {
    type: mongoose.ObjectId,
    ref:'Product'
  },
  plan: {
    type: mongoose.ObjectId,
    ref: 'Plan'
  }
})

planSchema.statics.findByStripeId = function(stripeId){
  return this.findOne({stripeId})
}

planSchema.statics.findActivePlan = function(plan) {
  return this.findOne({plan,active: true})
}



module.exports = mongoose.model('StripePlan',planSchema,`plans_${db.prefix}`)
