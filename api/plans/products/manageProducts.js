//Stripe
//Erros e error handler
const { customError } = require('../../../common/errors')
const { stripe } = require('../../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)
const Product = require('./products.model')
const Plan = require('../plans.model')

const stripeProductFromPlan = async (planId) =>{
  const productInfo = await Product.findByPlan(planId)
  if(!productInfo){
    const planInfo = await Plan.findById(planId)
    if(!planInfo){
      throw customError(400,'Plan not found.')
    }

    const stripeProduct = await stripeApi.products.create({
      name: planInfo.name,
      type: 'service'
    })
    const product = new Product({
      stripeId: stripeProduct.id,
      name: stripeProduct.name,
      planId
    })
    const savedProduct = await product.save()
    return product
  }
  else{
    return productInfo
  }
}

exports.stripeProductFromPlan = stripeProductFromPlan
