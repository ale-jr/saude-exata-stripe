const mongoose = require('mongoose')
const { db } = require('../../../common/environment')

const productSchema = new mongoose.Schema({
  stripeId: String,
  name: String,
  planId: {
    type: mongoose.ObjectId,
    unique: true
  },
})

productSchema.statics.findByPlan = function(planId){
  return this.findOne({planId})
}


module.exports = mongoose.model('Product',productSchema,`products_${db.prefix}`)
