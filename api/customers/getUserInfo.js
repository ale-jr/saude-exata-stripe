//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

//Modelos do banco de dados
const User = require('./users.model')
const Customer = require('./customers.model')
const Card = require('../cards/model')





const getUser = async (userId) => {
  //Obter id do stripe
  const customer = await Customer.findByUserId(userId).populate('User').lean()

  //Caso o usuário não seja cadastrado no stripe
  if(!customer){
    const error = new Error('Customer not found, create a customer account first')
    error.json = {
      message: 'Customer not found, create a customer account first'
    }
    error.http_code = 404
    throw error
  }

  //Obtém cartões salvos no nosso banco de dados
  const userCards = await Card.findUserCards(userId).lean()

  //Obtem dados do Stripe
  const stripeCustomer = await stripeApi.customers.retrieve(customer.stripeId)

  //Mapear os cartões para achar o cartão principal
  const cards = userCards.map((card)=>({
    ...card,
    default_source: card.cardId == stripeCustomer.default_source
  }))

  //retorna dados do cliente
  return {
    ...customer,
    cards,
    stripe: stripeCustomer
  }
}
exports.getUser = getUser

exports.getCards = async (userId) => {
  //Obtem as informações de getUser
  const {cards} = await getUser(userId)

  //retorna lista de cartões
  return cards
}
