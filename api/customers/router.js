//Rotas do express
const router = require('express').Router()
//Erros e error handler
const { customError } = require('../../common/errors')
const { asyncMiddleware } = require('../../common/utils')



//Middleware para autorização
const { authorize, authorizeAdmin } = require('../../security/authz.handler')


//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

//Modelos do banco de dados
const User = require('./users.model')
const Customer = require('./customers.model')
const Card = require('../cards/model')

//obter cartões de um usuário
const { getCards, getUser } = require('./getUserInfo')

//Caminho da
const path = '/customers'


//Endpoint para criação do cliente no Stripe
const createCustomer = async (req,resp) => {

  const userId = req.params.id || req.authenticated._id
  let email = req.body ? req.body.email : undefined

  //Caso o email não tenha sido enviado, usa o email do userId
  if(!email){
    const user = await User.findById(userId)
    email = user.email
  }

  //Verifica se já existe um usuário no Stipe para aquele usuário
  const hasCustomer = await Customer.findOne({userId})
  if(hasCustomer){
    //Se tiver usuário, retorna um erro
    throw customError(409,'Customer already created',{customer:hasCustomer})
  }

  //Cria usuário no stripe
  const stripeCustomer = await stripeApi.customers.create({
    email,
    metadata: {
      userId: String(userId)
    }
  })

  //Salva cliente no nosso banco de dados
  const customer = new Customer({
    email,
    userId,
    stripeId: stripeCustomer.id
  })
  const savedCustomer = await customer.save()

  //Retorna dados do cliente criado
  resp.json(savedCustomer)
}
//fim createCustomer


//Obtem os cartões do usuário
const getUserCards = async (req,resp) => {
  const userId = req.params.id || req.authenticated._id
  const cards = await getCards(userId)
  resp.json(cards)
}


//Obtem informações do usuário
const getUserInfo = async (req,resp) => {
  const userId = req.params.id || req.authenticated._id
  const info = await getUser(userId)
  resp.json(info)
}


//Define o cartão padrão
const setDefaultCard = async (req,resp) => {
  const userId = req.params.id || req.authenticated._id
  const cardId = req.body.cardId

  //Verificar se o id do cartão está em body
  if(!cardId){
    throw customError(400,'Card field is required.',{customer:hasCustomer})
  }
  //Obter id do stripe
  const customer = await Customer.findByUserId(userId)

  //Caso o usuário não seja cadastrado no stripe
  if(!customer){
    throw customError(404,'Customer not found',{customer:hasCustomer})
  }

  //Obter os detalhes do cartão
  const stripeCard = await Card.findOne({_id: cardId, userId})

  //Se o cartão não for encontrado, voltar um erro
  if(!stripeCard){
    throw customError(400,'Card not found.')
  }

  //Atualiza o cartão padrão
  await stripeApi.customers.update(
    customer.stripeId,
    {
      default_source: stripeCard.cardId
    }
  )

  //Tudo certo :D
  resp.send(204).end()
}
//fim setDefaultCard


//DEFINIÇÃO DE ENDPOINTS

//Rotas para o usuário logado

router.post('/',[authorize,asyncMiddleware(createCustomer)])
router.get('/',[authorize,asyncMiddleware(getUserInfo)])
router.get('/cards',[authorize,asyncMiddleware(getUserCards)])
router.post('/cards/default',[authorize,asyncMiddleware(setDefaultCard)])

//FIM - Rotas para o usuário logado

//Rotas apenas para o admin

//Cria o stripe customer para um usuário em específico
router.post('/:id',[authorizeAdmin(),asyncMiddleware(createCustomer)])
router.get('/:id',[authorizeAdmin(),asyncMiddleware(getUserInfo)])
router.get('/:id/cards',[authorizeAdmin(),asyncMiddleware(getUserCards)])
router.post('/:id/cards/default',[authorizeAdmin(),asyncMiddleware(setDefaultCard)])

//FIM - rotas apenas para o admin

//Exporta rotas e o caminho padrão
exports.router = router
exports.path = path
