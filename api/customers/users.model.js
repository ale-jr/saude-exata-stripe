const mongoose = require('mongoose')


const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  surename: {
    type: String,
    required: true
  },
  profiles: {
    type: [mongoose.ObjectId],
    required: false
  },
  email: {
    type: String,
    required: true
  },
})


//Encontrar usuário pelo email
userSchema.statics.findByEmail = function(email,projection){
  return this.findOne({email},projection)
}

//Verificar perfis do usuário
userSchema.methods.hasAny = function(...profiles){
  return profiles.some(profile=> this.profiles.indexOf(profile) !== -1)
}


module.exports = mongoose.model('User',userSchema,'users')
