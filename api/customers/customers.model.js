const mongoose = require('mongoose')
const { db } = require('../../common/environment')

const customerSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  stripeId: {
    type: String,
    required: true
  },
  userId: {
    type: mongoose.ObjectId,
    required: true,
    ref: 'User'
  }
})

customerSchema.statics.findByUserId = function(userId){
  return this.findOne({userId})
}

module.exports = mongoose.model('Customer',customerSchema,`customers_${db.prefix}`)
