//Rotas do express
const router = require('express').Router()
//Erros e error handler
const { customError } = require('../../common/errors')
const { asyncMiddleware } = require('../../common/utils')


//Middleware para autorização
const { authorize } = require('../../security/authz.handler')

//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)


//Modelos do bando de dados
const Card = require('./model')
const Customer = require('../customers/customers.model')


//Caminho das rotas desse arquivo
const path = '/cards'

//Criar cartão a partir de um token
const createCardFromToken = async (req,resp,next) => {
  const userId = req.params.id || req.authenticated._id
  const token = req.body.token
    //Caso não haja um token no corpo da requisição
    if(!token){
      throw customError(400,'Token is required.')
    }

    //Obtem os dados do cliente do Stripe
    const customer = await Customer.findByUserId(userId)

    //Caso não haja um cliente criado, retornar um erro
    if(!customer){
      throw customError(400,'Customer does not exists, create a customer account first')
    }

    //Insere o cartão na lista de cartões do cliente no Stripe
    const stripeCard = await stripeApi.customers.createSource(customer.stripeId,{
      source: token
    })

    //Salva dados do cartão no nosso banco de dados
    const card = new Card({
      cardId: stripeCard.id,
      brand: stripeCard.brand,
      exp_month: stripeCard.exp_month,
      exp_year: stripeCard.exp_year,
      last4: stripeCard.last4,
      funding: stripeCard.funding,
      name: stripeCard.name,
      stripeCustomerId: stripeCard.customer,
      userId
    })
    const savedCard = await card.save()

    //Retorna dados do cartão recem inserido
    resp.json(savedCard)
}

router.post('/',[authorize,asyncMiddleware(createCardFromToken)])

exports.router = router
exports.path = path
