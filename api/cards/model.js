const mongoose = require('mongoose')
const { db } = require('../../common/environment')

const cardSchema =  new mongoose.Schema({
  cardId: String,
  brand: String,
  exp_month: Number,
  exp_year: Number,
  last4: String,
  funding: String,
  name: String,
  userId: {
    type: mongoose.ObjectId,
    required: true,
    ref: 'User'
  },
  stripeCustomerId: String
})

cardSchema.statics.findUserCards = function(userId){
  return this.find({userId})
}

module.exports = mongoose.model('Card',cardSchema,`cards_${db.prefix}`)
