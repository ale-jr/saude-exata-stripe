//Rotas do express
const router = require('express').Router()

//Cálculo de datas
const moment = require('moment')

//Erros e error handler
const { customError } = require('../../common/errors')
const { asyncMiddleware } = require('../../common/utils')


//Middleware para autorização
const { authorize, authorizeAdmin } = require('../../security/authz.handler')


//Stripe
const { stripe } = require('../../common/environment')
const stripeApi = require('stripe')(stripe.secret_key)

//Modelos do banco de dados
const Plan = require('../plans/plans.model')
const StripePlan = require('../plans/stripePlans.model')
const Subscription = require('./model')
//obter cartões de um usuário
const { getCards, getUser } = require('../customers/getUserInfo')



//Caminho das rotas
const path = '/subscriptions'

//Criar assinatura
const createSubscription = async (req,resp,next) =>{
    const userId = req.params.id || req.authenticated._id
    const { planId, card } = req.body
    let default_source = undefined
    //Obter detalhes do cliente no Stripe
    const customer = await getUser(userId)

    //Definir o cartão de crédito
    if(card){
      const customerHasCard = customer.cards.some((c)=>{
        if(c._id == card){
          default_source = c.cardId
          return true
        }
        return false
      })

      if(!customerHasCard){
        throw customError(400,'Card not found.')
      }
    }

    //Caso não haja um cliente no Stripe
    if(!customer){
      throw customError(400,'Customer not found')
    }


    //Obter detalhes do plano selecionado
    const activePlan = await StripePlan.findActivePlan(planId)
    const plan = await Plan.findById(planId)

    //Caso o plano não exista
    if(!activePlan || !plan){
      throw customError(400,'Plan not found')
    }

    //definir data de cancelamento
    const cancel_at = moment().add(plan.months,'M').unix()

    //Criar assinatura no Stripe
    const stripeSubscription = await stripeApi.subscriptions.create({
      customer:customer.stripeId,
      cancel_at,
      default_source,
      items: [
        {plan: activePlan.stripeId }
      ]
    })

    //Salvar detalhes da assinatura no nosso banco de dados
    const subscription = new Subscription({
      stripeId: stripeSubscription.id,
      customer: customer.stripeId,
      stripePlan: activePlan.stripeId,
      cancel_at,
      status: stripeSubscription.status,
      userId,
      planId,
      card
    })

    //Salvar e retornar dados da assinatura
    const savedSubscription = await subscription.save()
    resp.json(savedSubscription)
}

//Atualiza dados assinatura
const updateSubscription = async (req,resp,next) => {

    const userId = req.params.id || req.authenticated._id
    const subscriptionId = req.params.subscription

    const { planId, card } = req.body

    //Campos que vão ser atualizados
    let updateFields = {}

    //Obter detalhes do cliente no Stripe
    const customer = await getUser(userId)
    //Caso não haja um cliente no Stripe
    if(!customer){
      throw customError(400,'Customer not found')
    }

    //Definir o cartão de crédito caso seja necessário alterar
    if(card){
      const customerHasCard = customer.cards.some((c)=>{
        if(c._id == card){
          //Define o novo cartão para fazer a atualização
          updateFields.default_source = c.cardId
          return true
        }
        return false
      })

      //Caso o cartão não seja encontrado
      if(!customerHasCard){
        throw customError(400,'Card not found')
      }
    }

    //Obter detalhes da inscrição no plano
    const subscription = await Subscription.findById(subscriptionId)

    //Caso a assinatura nao seja encontrada
    if(!subscription){
      throw customError(400,'Subscription not found')
    }

    //Verifica se a assinatura pertence ao usuário
    if(String(userId) !== String(subscription.userId)){
      throw customError(400,'Subscription does not belong to the customer')
    }


    let activePlan = null
    let plan =  null
    //Caso o usuário queira mudar o plano, alterar o plano
    if(planId){
      //Obter detalhes do plano selecionado
      activePlan = await StripePlan.findActivePlan(planId)
      plan = await Plan.findById(planId)

      //Caso o plano não exista
      if(!activePlan || !plan){
        throw customError(400,'Plan not found')
      }

      //definir data de cancelamento
      const cancel_at = moment().add(plan.months,'M').unix()

      //Obtem assinatura atual do stripe
      const currentSubscription = await stripeApi.subscriptions.retrieve(subscription.stripeId)
      //atualiza esses dados
      updateFields.items = [{
        id: currentSubscription.items.data[0].id,
        plan: activePlan.stripeId
      }]
      updateFields.cancel_at = cancel_at
    }

    //Finalmente, atualiza o que precisa ser atualizado
    const stripeSubscription = await stripeApi.subscriptions.update(subscription.stripeId,updateFields)

    //Atualiza no nosso bd também
    const updatedSubscription = await Subscription.findOneAndUpdate({_id:subscriptionId},{
      stripePlan: stripeSubscription.plan.id,
      cancel_at : stripeSubscription.cancel_at,
      status: stripeSubscription.status,
      planId : planId ? planId : subscription.planId,
      card : card ? card : subscription.card
    },{new: true, useFindAndModify: true})

    //retorna o plano atualizado
    resp.json(updatedSubscription)
}

//Endpoint para cancelar uma assinatura
const deleteSubscription = async (req,resp,next) => {
    const userId = req.params.id || req.authenticated._id
    const subscriptionId = req.params.subscription

    //Obter detalhes do cliente no Stripe
    const customer = await getUser(userId)
    //Caso não haja um cliente no Stripe
    if(!customer){
      throw customError(400,'customer not found')
    }

    //Obter detalhes da inscrição no plano
    const subscription = await Subscription.findById(subscriptionId)

    //Caso a assinatura não exista
    if(!subscription){
      throw customError(400,'Subscription not found')
    }

    //Verifica se a assinatura pertence ao usuário
    if(String(userId) != String(subscription.userId)){
      throw customError(400,'Subscription does not belong to the customer')
    }

    //Era uma vez uma assinatura
    const stripeSubscription = await stripeApi.subscriptions.del(subscription.stripeId)

    //Atualiza a assinatura no nosso BD
    const updatedSubscription = await Subscription.findOneAndUpdate({_id:subscriptionId},{
      cancel_at: stripeSubscription.cancel_at,
      canceled_at: stripeSubscription.canceled_at,
      status: stripeSubscription.status,
    },{new: true, useFindAndModify: true})

    //retorna os dados atualizados da assinatura
    resp.json(updatedSubscription)
}


//obtem as assinaturas de um usuário
const getSubscriptions = async (req,resp,next) => {

  const userId = req.params.id || req.authenticated._id

    //Busca as assinaturas
    const subscriptions = await Subscription.findUserPlans(userId)
    //retorna os dados da assinatura
    resp.json(subscriptions)
}


//Rotas para usuários
router.get('/',[authorize,asyncMiddleware(getSubscriptions)])
router.post('/',[authorize,asyncMiddleware(createSubscription)])
router.post('/:subscription/',[authorize,asyncMiddleware(updateSubscription)])
router.delete('/:subscription/',[authorize,asyncMiddleware(deleteSubscription)])


//Rotas para o admin
router.get('/byUser/:id/',[authorizeAdmin(),asyncMiddleware(getSubscriptions)])
router.post('/byUser/:id/',[authorizeAdmin(),asyncMiddleware(createSubscription)])
router.post('/byUser/:id/:subscription/',[authorizeAdmin(),asyncMiddleware(updateSubscription)])
router.delete('/byUser/:id/:subscription/',[authorizeAdmin(),asyncMiddleware(deleteSubscription)])


exports.router = router
exports.path = path
