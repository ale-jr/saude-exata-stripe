const mongoose = require('mongoose')
const { db } = require('../../common/environment')

const subscriptionSchema = new mongoose.Schema({
  stripeId: String,
  customer: String,
  stripePlan: String,
  cancel_at: Number,
  canceled_at: Number,
  status: String,
  userId: {
    type: mongoose.ObjectId,
    ref: 'User'
  },
  planId: {
    type: mongoose.ObjectId,
    ref: 'Plan'
  },
  card: {
    type: mongoose.ObjectId,
    ref: 'Card'
  }
})

subscriptionSchema.statics.findUserPlans = function(userId){
  return this.find({userId}).populate('Plan')
}

module.exports = mongoose.model('Subscription',subscriptionSchema,`subscriptions_${db.prefix}`)
