# Saúde Exata - Integração com Stripe
A documentação da API está em [https://app.swaggerhub.com/apis/ale-jr/Saude-Exata-Stripe/1.0.0#/](https://app.swaggerhub.com/apis/ale-jr/Saude-Exata-Stripe/1.0.0#/)

## Variáveis de ambiente
A aplicação utiliza algumas variáveis de ambiente, caso tenha algum conflito com o nome de alguma variável? Basta trocar no arquivo [environment.js](./common/environment.js), tem também um .json ([environment.json](./environment.json)) com um template contendo todas as variáveis utilizadas ;D

## Estrutura do projeto
- ./index.js: Inicia tudo da aplicação e é onde você pode registrar novos routers
- ./environment.json ou nodemon.json : Onde estão as variáveis de ambiente
#### /server
- server.js: inicia o servidor e o mongoose, também é onde você pode inserir outros middlewares (na função initializeServer)
- error.handler.js: middleware que gerencia os erros, onde você pode colocar um logger (essa aplicação usa bunyan para logar dados) ou algo do tipo
#### /security
- authz.handler.js: lida com as autorizações (usa os dados do req.authenticated que são gerados no middleware tokenParser), existem algumas formas de autorizar uma rota, consulte o arquivo para mais informações
- token.parser.js: middleware responsável por verificar o JWT no header de autorização, bem como preencher o objeto req.authenticated com detalhes do usuário
#### /common
- environment.js: variáveis de ambiente
- errors.js: lida com os erros nas requisições e tem uma função para criar erros com código http e json
- logger.js: cria e configura o bunyan, pode ser infortado para qualquer arquivo que precise salver logs
- utils.js: alguns middlewares e ferramentas ;)
#### /api
É onde as rotas estão, cada subdiretório contém um arquivo com as rotas (router.js) e se necessário, arquivo com o schema para o mongoose (model.js)

## Webhook
  Toda vez que a aplicação é iniciada, o webhook é recriado. A inicialização do webhook está em api/webhooks/initialize.js e o endpoint dele está em /api/webhooks/router.js

## A fazer
- Definir qual vai ser o comportamento da aplicação quando uma assinatura tem um problema com o pagamento (O webhook está feito, só falta saber como notificar o usuário)


## Dúvidas?
Entre em contato por esses meios:
- Email: contato@alejr.com.br
- Whatsapp: +55 (11) 93257-2107

Muito obrigado :D
