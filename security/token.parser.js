const jwt = require('jsonwebtoken')
const { security } = require('../common/environment')


//Modelo de usuários (já existe na plataforma)
const User = require('../api/customers/users.model')

//Extrai informaçoes do token jwt passado na requisição, se não tiver nenhum token, a requisição segue e quem lida com a falha na autenticação vai ser o middleware de autenticação
exports.tokenParser = (req,resp,next) => {
  //Extrai o token do request
  const token = extractToken(req)
  if(token){
    //applyBearer continua con a requisição
    jwt.verify(token,security.jwt_secret,applyBearer(req,next))
  }
  else{
    next()
  }
}

//Extrai o token com esse modelo
//Authorization: Bearer eyJhbGciOiJIUzM4NCJ9.e30.VuTPhOlct37OU7HTvf0rv667dBBxukhR3YLBpLuHj70RQzGBpAM4hi4r-t1SCGys
const extractToken = (req) => {
  let token = undefined
  const authorization = req.header('authorization')
  if(authorization){
    const parts = authorization.split(' ')
    if(parts.length === 2 && parts[0] === 'Bearer'){
      token = parts[1]
    }
  }
  return token
}


//Recebe a resposta de verificação do jwt.verify
const applyBearer = (req,next) => (error,decoded) => {
    if(decoded){
      User.findById(decoded.id)
      .then(user=>{
        if(user){
          req.authenticated = user
        }
        next()
      })
      .catch(next)
    }
}
