
const {ForbiddenError} = require('../common/errors')

//Middleware para verificar o usuário possui o perfil para continuar com a requisição
const authorizeByProfile = (...profiles) => (req,resp,next) => {
  if(req.authenticated !== undefined && req.authenticated.hasAny(...profiles)){
    next()
  }
  else{
    ForbiddenError(resp)
  }
}

//Middleware para verificar se o usuário está logado
const authorize = (req,resp,next) => {
  if(req.authenticated !== undefined){
    next()
  }
  else{
    ForbiddenError(resp)
  }
}

//TODO: descobrir como funcionam as contas de admin
const authorizeAdmin = () => (req,resp,next) => {
  if(req.authenticated !== undefined){
    next()
  }
  else{
    ForbiddenError(resp)
  }
}


exports.authorizeByProfile = authorizeByProfile
exports.authorize = authorize
exports.authorizeAdmin = authorizeAdmin
